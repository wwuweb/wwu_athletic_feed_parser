<?php

class AthleticFeedParser extends FeedsParser {

  /**
   * Return mapping sources.
   *
   */
  public function getMappingSources() {
    return array(
      'event_name' => array(
        'name' => t('Event Name'),
        'description' => t(''),
      ),
      'team' => array(
        'name' => t('Team'),
        'description' => t('The relevent team. E.g. "m-baskbl" = Men\'s Basketball.'),
      ),
      'tournament' => array(
        'name' => t('Tournament'),
        'description' => t('Name of the tournament.'),
      ),
      'location' => array(
        'name' => t('Location'),
        'description' => t(''),
      ),
      'date_et' => array(
        'name' => t('Date (Eastern Time)'),
        'description' => t(''),
      ),
      'date_pt' => array(
        'name' => t('Date (Pacific Time)'),
        'description' => t(''),
      ),
      'date_local' => array(
        'name' => t('Date (Local Time)'),
        'description' => t('Date and time at location where event occurs.'),
      ),
      'local_timezone' => array(
        'name' => t('Local Timezone'),
        'description' => t('Timezone of location where event occurs.'),
      ),
      'id' => array(
        'name' => t('Event ID'),
        'description' => t(''),
      ),
      'hc' => array(
        'name' => t('Home Team Code'),
        'description' => t(''),
      ),
      'vc' => array(
        'name' => t('Visiting Team Code'),
        'description' => t(''),
      ),
      'hn' => array(
        'name' => t('Home Team Name'),
        'description' => t(''),
      ),
      'vn' => array(
        'name' => t('Visiting Team Name'),
        'description' => t(''),
      ),
      'hs' => array(
        'name' => t('Home Team Score'),
        'description' => t(''),
      ),
      'vs' => array(
        'name' => t('Visiting Team Score'),
        'description' => t(''),
      ),
      'headtohead' => array(
        'name' => t('Head-to-head'),
        'description' => t('Whether or not event is "head-to-head".'),
      ),
      'gametracker' => array(
        'name' => t('Gametracker'),
        'description' => t(''),
      ),
      'gameaudio' => array(
        'name' => t('Game Audio'),
        'description' => t('Whether or not game audio is available.'),
      ),
      'gamevideo' => array(
        'name' => t('Game Video'),
        'description' => t('Whether or not game video is available.'),
      ),
    ) + parent::getMappingSources();
  }
  
  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
  	//Get team name from the feed url
  	//e.g. get "c-xc" out of "http://www.wwuvikings.com/data/xml/events/c-xc/2015/index.xml"
  	$feed_url = $fetcher_result->getFeedUrl();
	$team = "";
  	$pattern = "/.+wwuvikings\.com.+\/(.+)\/.+\/.+$/";
  	if (preg_match($pattern, $feed_url, $matches)) {
  		$team = $matches[1];
  	}
  	 
	$items = array();
  	$game_days = @new SimpleXMLElement($fetcher_result->getRaw());
  	$events = $game_days->xpath('//event'); //Every <event> will become a node/row in return array
  	
  	
  	foreach ($events as $event) {
  		$row = array();
  		$row['team'] = $team;
  		
  		//Convert dates to proper format (YYYY-MM-DD HH:MM:SS) NOTE: date fields must use "UTC" as timezone conversion option
  		$et_pieces = $this->split_datetime($event['eastern_date'], $event['eastern_time']);
  		$row['date_et'] = $et_pieces['string'];
  		$row['et_timezone'] = "America/New_York";
  		
  		$local_date_raw = (string) $event->xpath("parent::*/@date")[0]; //Local date is stored in parent element
  		$local_pieces = $this->split_datetime($local_date_raw, $event['local_time']);
  		$row['date_local'] = $local_pieces['string'];
  		$row['local_timezone'] = $local_pieces['timezone'];
  		
  		//Use DateTime class to convert to pacific time from given eastern time
  		$datetime = new DateTime($et_pieces['string'], new DateTimezone('America/New_York'));
  		$datetime->setTimezone(new DateTimezone('America/Los_Angeles'));
  		$format = $local_pieces['all_day'] ? "Y-m-d 00:00:00" : "Y-m-d H:i:s";
  		$row['date_pt'] = $datetime->format($format);
  		$row['pt_timezone'] = "America/Los_Angeles";
  		
  		//Build field/value pairs for each attribute in the <event> element
  		foreach($event->attributes() as $key => $value) {
  			$row[$key] = (string) $value;
  		}
  		
  		$items[] = $row;
  	}
  	
  	$result = new FeedsParserResult($items);
  	return $result;
  }
  
  //Convert date and time given in "YYYYMMDD" and "HH:MM am|pm (timezone)" to YYYY-MM-DD HH:MM:SS
  //Also return the timezone in a format accepted by PHP (default easten time) and whether or not
  //the event occurs all day (i.e. a starting time could not be extracted)
  private function split_datetime($datestring,$timestring) {
  	//HT should be 2:00pm MT should be 7:00 pm
  	$timezones = array(' PT' => 'America/Los_Angeles', ' CT' => 'America/Chicago',
  					   ' HT' => 'Pacific/Honolulu', ' MT' => 'America/Denver',
  					   ' AKT' => 'America/Anchorage');
  	$pattern = "/(\d?\d):(\d\d) ?(..)?( .+)?/";
  	$hour = "00";
  	$minute = "00";
  	$timezone = "America/New_York";
  	$all_day = FALSE;
  	if (preg_match($pattern, $timestring, $matches)) {
  		$hour = $matches[1];
  		$minute = $matches[2];
  		if(count($matches) > 3 & $matches[3] == "PM") {
  			$hour += 12;
  		} else if ($hour < 10) {
  			$hour = '0' . $hour;
  		}
  		if (count($matches) > 4) {
  			$timezone = $matches[4];
  		}
  	} else {
  		$all_day = TRUE;
  	}
  	$year = substr($datestring, 0, 4);
  	$month = substr($datestring, 4, 2);
  	$day = substr($datestring, 6, 2);
  	
  	$date = array();
  	$date['timezone'] = array_key_exists($timezone, $timezones) ? $timezones[$timezone] : $timezone;
  	$date['string'] = $year . '-' . $month . '-' . $day . ' ' . $hour . ':' . $minute . ':00';
  	$date['all_day'] = $all_day;
  	return $date;
  }

}