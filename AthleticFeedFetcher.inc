<?php

class WWUFeedsHTTPFetcherResult extends FeedsHTTPFetcherResult {
	
	public function getFeedUrl() {
    	return $this->url;
  	}
	
}

class AthleticFeedFetcher extends FeedsHTTPFetcher {

	/**
	 * Implements FeedsFetcher::fetch().
	 */
	public function fetch(FeedsSource $source) {
		$source_config = $source->getConfigFor($this);
		if ($this->config['use_pubsubhubbub'] && ($raw = $this->subscriber($source->feed_nid)->receive())) {
			return new FeedsFetcherResult($raw);
		}
		$fetcher_result = new WWUFeedsHTTPFetcherResult($source_config['source']);
		// When request_timeout is empty, the global value is used.
		$fetcher_result->setTimeout($this->config['request_timeout']);
		return $fetcher_result;
	}

}